import random
import string

import pytest

from yara_guard.YaraGuard import YaraGuard, YaraGuardArgException, YaraGuardSources


@pytest.fixture
def random_str():
    yield "".join(random.choice(string.ascii_letters) for _ in range(10))


class TestYaraGuard:
    def test_config_file(self, random_str, tmp_path):

        config = tmp_path / f"{random_str}.toml"
        with pytest.raises(YaraGuardArgException, match="does not exist") as e:
            yg = YaraGuard(config_file=config)

        code = 2
        assert (
            e.value.code == code
        ), f"Initializing with non-existent config file exits with status “{code}”"

        with pytest.raises(YaraGuardArgException, match="not readable") as e:
            yg = YaraGuard(config_file="/etc/shadow")

        code = 13
        assert (
            e.value.code == code
        ), f"Initializing with non-readable config file exits with status “{code}”"

        config.touch(exist_ok=False)
        yg = YaraGuard(config_file=config)

        assert (
            yg.config_file == config
        ), "Initializing with existing file set’s file in class"

    def test_load_config(self, tmp_path):
        pass

    def test_sources(self, tmp_path, random_str):

        config = tmp_path / f"{random_str}.toml"
        config.touch(exist_ok=False)

        yg = YaraGuard(config_file=config)

        # Default to none
        assert len(yg.sources) == 0, "Sources defaults to an empty list"

        s1 = {"enabled": True, "name": "foo", "directory": "/foo"}
        s2 = {"enabled": False, "name": "bar", "directory": "/bar"}
        yg.sources = (s1, s2)
        for a, b in zip(yg.sources, (s1, s2)):
            assert isinstance(
                a, YaraGuardSources
            ), "Sources returns an instance of “YaraGuardSources”"
            assert a.enabled == b["enabled"], "Sources “enabled” input/output matches"
            assert a.name == b["name"], "Sources “name” input/output matches"
            assert (
                a.directory == b["directory"]
            ), "Sources “directory” input/output matches"
