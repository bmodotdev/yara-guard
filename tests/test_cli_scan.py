import os
import random
import string

import click
import pytest
from click.testing import CliRunner

from yara_guard.cli import cli
from yara_guard.commands.scan import scan
from yara_guard.YaraGuardScan import YaraGuardScan


@pytest.fixture
def runner():
    yield CliRunner()


@pytest.fixture
def random_str():
    yield "".join(random.choice(string.ascii_letters) for _ in range(10))


class TestCliScan:
    def test_help(self, runner):
        """
        Test --help in “yg scan --help”
        """

        result = runner.invoke(scan, ["scan", "--help"])
        assert result.exit_code == 0, f"“yg scan --help” exits with zero status"
        assert "Usage" in result.output, "String “Usage” appears"

    def test_config_file(self, runner, tmp_path, random_str):
        """
        Test --config-file in “yg --config-file foo.toml scan tmp_dir/”
        """

        # Setup
        opt = "--config-file"
        config_file = tmp_path / "config.toml"

        rules_dir = tmp_path / "rules"
        rules_dir.mkdir()

        rule_file_short = rules_dir / "dummy1.yar"
        rule_file_short.touch(exist_ok=False)

        rule_file_long = rules_dir / "dummy2.yara"
        rule_file_long.touch(exist_ok=False)

        search_dir = tmp_path / "search"
        search_dir.mkdir()

        target_file = search_dir / "dummy.txt"
        target_file.touch(exist_ok=False)

        # Non-existent config file
        result = runner.invoke(cli, [opt, str(config_file), "scan", str(search_dir)])

        assert (
            result.exit_code == 2
        ), f"Option “{opt}” with non-existent config file exits status “2”"

        assert "does not exist" in result.output, f"“does not exist” appears in output"

        # Existing non-readable config file
        result = runner.invoke(cli, [opt, "/etc/shadow", "scan", str(search_dir)])

        assert (
            result.exit_code != 0
        ), f"Option “{opt}” with non-readable config file exits status “13”"

        assert "not readable" in result.output, "“not readable” appears in output"

        # Existing empty config file
        config_file.touch(exist_ok=False)
        result = runner.invoke(cli, [opt, str(config_file), "scan", str(search_dir)])

        assert (
            result.exit_code == 0
        ), f"Option “{opt}” with existing config file exits with zero status"

        assert (
            str(target_file) in result.output
        ), f"Option “{opt}” with existing config file exits with no output"

        # Populate config file
        config_file.write_text(
            f"""
            [[sources]]
            enabled=true
            name="{random_str}"
            directory="{str(rules_dir)}"
        """
        )

        # Populate dummy rule files
        rule_file_short.write_text(f"rule {random_str}_short " + "{ condition: true }")
        rule_file_long.write_text(f"rule {random_str}_long " + "{ condition: true }")

        result = runner.invoke(cli, [opt, str(config_file), "scan", str(search_dir)])

        assert (
            result.exit_code == 0
        ), f"Option “{opt}” with dummy config exits status “0”"

        assert (
            f"{random_str}_short" in result.output
        ), f"Option “{opt}” with dummy config matches dummy rule from “.yar”"

        assert (
            f"{random_str}_long" in result.output
        ), f"Option “{opt}” with dummy config matches dummy rule from “.yara”"

    def test_threads(self, runner, tmp_path, monkeypatch):
        """
        Tests threads in “yg --config-file foo.toml scan --threads 2 tmp_dir/”
        """

        # Setup
        config_file = tmp_path / "config.toml"
        config_file.touch(exist_ok=False)

        search_dir = tmp_path / "search"
        search_dir.mkdir()

        # Threads greater than cpu_count
        cpu_count = 8
        threads = 16
        monkeypatch.setattr(os, 'cpu_count', lambda: cpu_count)

        result = runner.invoke(
            cli,
            [
                "--config-file",
                str(config_file),
                "scan",
                "--threads",
                threads,
                str(search_dir),
            ],
        )

        assert (
            result.exit_code != 0
        ), f"Option “--threads {threads}” exits with non-zero status when cpu_count “{cpu_count}”"

        assert (
            "Invalid Argument" in result.output
            and "must be an integer between" in result.output
        ), f"Option “--threads {threads}” gives warning when cpu_count “{cpu_count}”"

        # Threads less than cpu_count
        cpu_count = 16
        threads = 2

        result = runner.invoke(
            cli,
            [
                "--config-file",
                str(config_file),
                "scan",
                "--threads",
                threads,
                str(search_dir),
            ],
        )

        assert (
            result.exit_code == 0
        ), f"Option “--threads {threads}” exits with status “0” when cpu_count “{cpu_count}”"

        # Valid threads, processes, and cpu_count
        cpu_count = 16
        threads = 2
        procs = 0

        result = runner.invoke(
            cli,
            [
                "--config-file",
                str(config_file),
                "scan",
                "--threads",
                threads,
                "--processes",
                procs,
                str(search_dir),
            ],
        )

        assert (
            result.exit_code == 0
        ), f"Option “--threads {threads} --processes {procs}” exits with status “0” when cpu_count “{cpu_count}”"

        # Invalid threads and processes
        cpu_count = 16
        threads = 2
        procs = 2

        result = runner.invoke(
            cli,
            [
                "--config-file",
                str(config_file),
                "scan",
                "--threads",
                threads,
                "--processes",
                procs,
                str(search_dir),
            ],
        )

        assert (
            result.exit_code != 0
        ), f"Option “--threads {threads} --processes {procs}” exits with non-zero status when cpu_count “{cpu_count}”"

        assert (
            "incompatible" in result.output
        ), f"Option “--threads {threads} --processes {procs}” warns “incompatible” when cpu_count “{cpu_count}”"

        # Valid threads when cpu_count is 1
        cpu_count = 1
        threads = 1

        result = runner.invoke(
            cli,
            [
                "--config-file",
                str(config_file),
                "scan",
                "--threads",
                threads,
                str(search_dir),
            ],
        )

        assert (
            result.exit_code == 0
        ), f"Option “--threads {threads}” exits with status “0” when cpu_count “{cpu_count}”"

        # Probably a better way to do this, perhaps via monkeypatch.context
        monkeypatch.delattr(os, 'cpu_count')


    def test_processes(self, runner, tmp_path, monkeypatch):

        # Setup
        config_file = tmp_path / "config.toml"
        config_file.touch(exist_ok=False)

        search_dir = tmp_path / "search"
        search_dir.mkdir()

        # Procs greater than cpu_count
        cpu_count = 8
        procs = 16
        monkeypatch.setattr(os, 'cpu_count', lambda: cpu_count)

        result = runner.invoke(
            cli,
            [
                "--config-file",
                str(config_file),
                "scan",
                "--processes",
                procs,
                str(search_dir),
            ],
        )

        assert (
            result.exit_code != 0
        ), f"Option “--processes {procs}” exits with non-zero status when cpu_count “{cpu_count}”"

        assert (
            "Invalid Argument" in result.output
            and "must be an integer between" in result.output
        ), f"Option “--processes {procs}” gives warning when cpu_count “{cpu_count}”"

        # Valid threads, processes, and cpu_count
        cpu_count = 8
        procs = 2
        threads = 0

        result = runner.invoke(
            cli,
            [
                "--config-file",
                str(config_file),
                "scan",
                "--processes",
                procs,
                "--threads",
                threads,
                str(search_dir),
            ],
        )

        assert (
            result.exit_code == 0
        ), f"Option “--processes {procs} --threads {threads}” exits with status “0” when cpu_count “{cpu_count}”"

        # Valid processes and threads when cpu_count is 1
        cpu_count = 1
        procs = 1
        threads = 0

        result = runner.invoke(
            cli,
            [
                "--config-file",
                str(config_file),
                "scan",
                "--processes",
                procs,
                "--threads",
                threads,
                str(search_dir),
            ],
        )

        assert (
            result.exit_code == 0
        ), f"Option “--processes {procs}” exits with status “0” when cpu_count “{cpu_count}”"

        # Probably a better way to do this, perhaps via monkeypatch.context
        monkeypatch.delattr(os, 'cpu_count')

    def test_recursive(self, runner, tmp_path, random_str):
        """
        Test --recursive in “yg --config-file foo.toml scan --recursive tmp_dir/”
        """

        # Setup rules dir
        rules_dir = tmp_path / "rules"
        rules_dir.mkdir()

        rule_file = rules_dir / "dummy.yara"
        rule_file.write_text(f"rule {random_str} " + "{ condition: true }")

        # Setup config
        config_file = tmp_path / "config.toml"

        config_file.write_text(
            f"""
            [[sources]]
            enabled=true
            name="{random_str}"
            directory="{str(rules_dir)}"
        """
        )

        # Setup search dir
        search_dir = tmp_path / "search"
        search_dir.mkdir()

        # Setup target file
        target_file = search_dir / "target.txt"
        target_file.touch(exist_ok=False)

        # Setup nested target file
        nested_dir = search_dir / "nested"
        nested_dir.mkdir()
        nested_file = nested_dir / "nested.txt"
        nested_file.touch(exist_ok=False)

        result = runner.invoke(
            cli,
            [
                "--config-file",
                str(config_file),
                "scan",
                str(search_dir),
            ],
        )

        assert result.exit_code == 0, "Scan using dummy rule exits with status “0”"

        assert str(target_file) in result.output, "Target file found using dummy rule"

        assert (
            str(nested_file) not in result.output
        ), "Nested target not found when using dummy rule without “--recursive”"

        result = runner.invoke(
            cli,
            [
                "--config-file",
                str(config_file),
                "scan",
                "--recursive",
                str(search_dir),
            ],
        )

        assert (
            result.exit_code == 0
        ), "Recursive scan using dummy rule exits with status “0”"

        assert str(target_file) in result.output, "Target file found using dummy rule"

        assert (
            str(nested_file) in result.output
        ), "Nested target found when using dummy rule and “--recursive”"
