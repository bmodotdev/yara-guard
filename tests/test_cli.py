import random
import re
import string

import click
import pytest
from click.testing import CliRunner

from yara_guard.cli import cli, help_text


@pytest.fixture
def runner():
    yield CliRunner()


@pytest.fixture
def random_str():
    yield "".join(random.choice(string.ascii_letters) for _ in range(10))


class TestCli:

    subcommands: tuple = ("scan",)

    def test_help(self, runner):
        """
        Test “yg --help”
        """

        opt = "--help"
        result = runner.invoke(cli, [opt])

        assert result.exit_code == 0, f"Option “{opt}” exits with zero status"
        assert help_text in result.output, f"String “{help_text}” appears"
        assert "Usage" in result.output, "String “Usage” appears"

        for cmd in self.subcommands:
            regex = r"^\s+" + cmd + r"\s*$"
            assert re.search(
                regex, result.output, flags=re.M
            ), f"Subcommand “{cmd}” appears"

    def test_unknown(self, runner, random_str, tmp_path):
        """
        Test unknown options
        """

        result = runner.invoke(cli, [f"--{random_str}"])

        assert (
            result.exit_code != 0
        ), f"Unknown option “--{random_str}” exits with non-zero status"

        regex = r"No such option.+" + random_str
        assert re.search(
            regex, result.output, flags=re.M
        ), f"No such option “--{random_str}” appears"

    def test_config_file(self, runner, tmp_path, random_str):
        """
        Test “yg --config-file”
        """

        for opt in ("--config-file", "-c"):

            # Ensure the director
            result = runner.invoke(cli, [opt])

            assert (
                result.exit_code != 0
            ), f"Option “{opt}” without an argument exits with non-zero status"

            regex = opt + r".+requires an argument"
            assert re.search(
                regex, result.output, flags=re.M
            ), f"Option “{opt}” requires an argument appears"

    def test_version(self):
        """
        Test “yg --version”
        """
        pass
