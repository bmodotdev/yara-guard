# YARA Guard
Create a YARA signature library composed of “sources” that compile rules from
directories or remote repositories. YARA Guard will use your signature library
to scan the filesystem or can be configured as a daemon to watch events on
specified paths and scan on demand.

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md)
[![pipeline status](https://gitlab.com/bmodotdev/yara-guard/badges/main/pipeline.svg)](https://gitlab.com/bmodotdev/yara-guard/-/commits/main)
[![coverage report](https://gitlab.com/bmodotdev/yara-guard/badges/main/coverage.svg)](https://gitlab.com/bmodotdev/yara-guard/-/commits/main)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)

# Active Development
This project is still under active development;
arguments, classes, dependencies, etc., may change.

# Dependencies
YARA Guard uses the official [yara-python](https://pypi.org/project/yara-python/)
module, which depends on the official [C library](https://github.com/VirusTotal/yara).
You can follow the [documentation](https://yara.readthedocs.io/en/latest/gettingstarted.html#compiling-yara)
to compile from source or on Ubuntu you can use apt:

```
$ sudo apt install libyara-dev
```

# Usage
`yg` makes use of subcommands:

```
$ poetry run yg --help
Usage: yg [OPTIONS] COMMAND [ARGS]...

  Watch and scan a filesystem with YARA rules

Options:
  -c, --config-file FILE  Path to config file  [default:
                          /etc/yaraguard/config.toml]
  -v, --verbose           Enable verbose output
  -V, --version           Print application version
  --help                  Show this message and exit.

Commands:
  scan
```

## Scanning
```
$ poetry run yg scan --help
Usage: yg scan [OPTIONS] PATHS...

Options:
  -p, --processes INTEGER         Number of scanners to run in parallel
                                  [default: 0]
  -t, --threads INTEGER           Number of scanners to run in threads
                                  [default: 16]
  -n, --nice INTEGER              Set scheduling priority, -20 to 19, highest
                                  to lowest priority
  -r, --recursive                 Scan directory recursively
  -m, --match-glob TEXT           Match files with glob(s), e.g. “-m '*.php'
                                  -m '*.py'
  -i, --ignore-glob TEXT          Exclude files glob(s), e.g. “-i
                                  node_modules”
  -D, --quarantine-dir DIRECTORY  Directory to move files matched by YARA
                                  rules
  -P, --quarantine-chmod TEXT     Permissions to set on files matched by YARA
                                  rules
  -O, --quarantine-chown TEXT     Owner and/or Group to set on files matched
                                  by YARA rules, i.e. “[user][:[group]]
  --help                          Show this message and exit.
```

### --processes
On CPU bound systems, you may choose to run scans in parallel processes.
Typically, YARA Guard is IO bound, so this setting defaults to 0.

This option is incompatible with `--threads`.

### --threads
On IO bound systems, you may choose to run scans in multiple threads.
This setting defaults to the number of cores available on the system.

This option is incompatible with `--processes`.

### --nice
unimplemented

### --recursive
By default, YARA Guard only searches files in the immediate directory.
Pass this argument to recursively scan the provided directories.

### --match-glob
This argument allows providing multiple POSIX glob patterns to identify
files to scan.

There is no logic to identify or resolve conflicting globs between
arguments `--match-glob` and `--ignore-glob`.
The argument `--ignore-glob` takes precedence over `--match-glob`.

### --ignore-glob
This argument allows providing multiple POSIX glob patterns to identify
files to ignore.

There is no logic to identify or  resolve conflicting globs between
arguments `--match-glob` and `--ignore-glob`.
The argument `--ignore-glob` takes precedence over `--match-glob`.

### --quarantine-dir
unimplemented

### --quarantine-chmod
unimplemented

### --quarantine-chown
unimplemented

# Docker
Currently, 4 images are available for this project,
hosted on GitLab's registry “registry.gitlab.com”:
* bmodotdev/yara-guard:dev-py38
* bmodotdev/yara-guard:main-py38
* bmodotdev/yara-guard:dev-py310
* bmodotdev/yara-guard:main-py310
* bmodotdev/yara-guard:latest   # alias main-py310

# Using docker images
For ease of development, the developer images have an `ENTRYPOINT` of `poetry`;
whereas the main images have an entry point of YARA Guard:

```
$ docker run --rm -it registry.gitlab.com/bmodotdev/yara-guard:dev-py310 --version
Poetry (version 1.4.0)

$ docker run --rm -it registry.gitlab.com/bmodotdev/yara-guard:latest --help
Usage: yg [OPTIONS] COMMAND [ARGS]...

  Watch and scan a filesystem with YARA rules

Options:
  -c, --config-file FILE  Path to config file  [default:
                          /etc/yaraguard/config.toml]
  -v, --verbose           Enable verbose output
  -V, --version           Print application version
  --help                  Show this message and exit.

Commands:
  scan
```

## Building docker images
The commands below can be used to build the supported images:

```
# Python 3.8 developer and main images
$ docker build -t registry.gitlab.com/bmodotdev/yara-guard:dev-py38 --target dev-py38 -f Dockerfile.py38 .
$ docker build -t registry.gitlab.com/bmodotdev/yara-guard:main-py38 --target main-py38 -f Dockerfile.py38 .

# Python 3.10 developer, main, and latest images
$ docker build -t registry.gitlab.com/bmodotdev/yara-guard:dev-py310 --target dev-py310 -f Dockerfile.py310 .
$ docker build -t registry.gitlab.com/bmodotdev/yara-guard:main-py310 --target main-py310 -f Dockerfile.py310 .
$ docker build -t registry.gitlab.com/bmodotdev/yara-guard:latest --target main-py310 -f Dockerfile.py310 .
```

# Contributing

This project has adopted the [code of conduct](CODE_OF_CONDUCT.md)
outlined by [Contributor Covenant](https://www.contributor-covenant.org/).

## Conventional Commits
[Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/#summary)
are used for [semantic versioning](https://semver.org/).

### Recognized Types
* build
* ci
* docs
* feat
* fix
* perf
* refactor
* style
* test

## Testing
The tests are located in `tests/` directory.
Tests can be executed directly via `pytest` or
in a virtual environment using `tox`.

### Pytest
Run the following command to execute the tests:

```
$ poetry run pytest --verbose
```

### Tox
Run the following command to execute the tests in a virtual environment
using multiple python versions:

```
$ poetry run tox
```

### Testing in docker
Only developer images have developer dependencies required for testing.
Developer docker images have an `ENTRYPOINT` of `poetry`:

```
$ docker run --rm -it registry.gitlab.com/bmodotdev/yara-guard:dev-py38 run pytest
$ docker run --rm -it registry.gitlab.com/bmodotdev/yara-guard:dev-py310 run pytest
```

## Style
[Black](https://black.readthedocs.io/en/stable/) is used for formatting.
Run the following command to to reformat code in directory `yara_guard/`:

```
$ poetry run black yara_guard/
```

## Type Checking
[mypy](https://mypy.readthedocs.io/en/stable/index.html) is used for type checking.
Run the following command to run static type checking:

```
$ poetry run mypy yara_guard/
```

## SAST Scanning
Static Application Security Testing is performed by
[bandit](https://bandit.readthedocs.io/en/latest/index.html).
Run the following command to scan with bandit:

```
$ poetry run bandit --configfile pyproject.toml --recursive --severity-level all yara_guard/
```
