#!/bin/sh

set -e

. ./venv/bin/activate

./venv/bin/yg "$@"
