# import glob
import yara # type: ignore
import concurrent.futures
import os
import pathlib
from typing import Iterable, Union

from yara_guard.YaraRules import YaraRules
from yara_guard.YaraGuard import YaraGuard, YaraGuardArgException, YaraGuardException


# class YaraGuardScanException(YaraGuardException):
#    prefix: str = "Invalid Argument:"
#
#    def __init__(self, *kwargs):
#        super().__init__(*kwargs)


class YaraGuardScan(YaraGuard):
    """This subclass of YaraGuard provides scanning functionality"""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.rules()

    __rules = None

    def rules(self):
        if not self.__rules:
            r = YaraRules(self.sources)
            self.__rules = r.compile()
        return self.__rules

    # The number of processes to run in parallel
    __processes: int = 1

    @property
    def processes(self) -> int:
        return self.__processes

    @processes.setter
    def processes(self, p: int) -> int:
        if p > 1 and self.threads > 1:
            raise YaraGuardArgException(
                "Argument “processes” is incompatible with argument “threads”"
            )
        cores = os.cpu_count() or 1
        if 0 <= p <= cores:
            self.__processes = p
            return self.__processes
        else:
            raise YaraGuardArgException(
                f"Argument “processes” must be an integer between “0” and “{cores}”"
            )

    # Needs python 3.11+
    #    # Max tasks per child process
    #    __max_tasks_per_child: Union[int, None] = None
    #    @property
    #    def max_tasks_per_child(self) -> Union[int, None]:
    #        return self.__max_tasks_per_child
    #
    #    @max_tasks_per_child.setter
    #    def max_tasks_per_child(self, t: int) -> Union[int, None]:
    #        if t is None or t > 0:
    #            self.__max_tasks_per_child = t
    #            return self.__max_tasks_per_child
    #        else:
    #            raise YaraGuardArgException("Arg “max_tasks_per_child” must be “None” or an integer greater than “0”")

    # Threads
    __threads: int = 1

    @property
    def threads(self) -> int:
        return self.__threads

    @threads.setter
    def threads(self, t: int) -> int:
        if t > 1 and self.processes > 1:
            raise YaraGuardArgException(
                "Argument “threads” is incompatible with argument “processes”"
            )
        cores = os.cpu_count() or 1
        if 0 <= t <= cores:
            self.__threads = t
            return self.__threads
        else:
            raise YaraGuardArgException(
                f"Argument “threads” must be an integer between “0” and “{cores}”"
            )

    # The process priority, -20 to 19
    __nice: int = 0

    @property
    def nice(self) -> int:
        return self.__nice

    @nice.setter
    def nice(self, n: int) -> Union[int, None]:
        if n is None:
            return None
        if -20 <= n < 20:
            self.__nice = n
            return self.__nice
        else:
            raise YaraGuardArgException(
                "Niceness must be an integer between -20 and 19 inclusive"
            )

    # Recursively search directories
    __recursive: bool = False

    @property
    def recursive(self) -> bool:
        return self.__recursive

    @recursive.setter
    def recursive(self, b: bool) -> bool:
        self.__recursive = b
        return self.__recursive

    # Match paths by glob
    #    __match_extension_globs: List[str] = field(default_factory=list)
    __match_globs: Iterable[str] = ()

    @property
    def match_glob(self) -> Iterable[str]:
        return self.__match_globs

    @match_glob.setter
    def match_glob(self, globs: Iterable[str]) -> Iterable[str]:
        self.__match_globs = tuple(globs)
        return self.__match_globs

    # Ignore paths by glob
    #    __ignore_file_globs: List[str] = field(default_factory=list)
    __ignore_globs: Iterable[str] = ()

    @property
    def ignore_glob(self) -> Iterable[str]:
        return self.__ignore_globs

    @ignore_glob.setter
    def ignore_glob(self, globs: Iterable[str]) -> Iterable[str]:
        self.__ignore_globs = globs
        return self.__ignore_globs

    # Watch files or directories
    def watch(self, paths: Iterable[str]):
        pass

    # Scan files or directories
    def scan(self, paths: Iterable[str]):
        paths = tuple(paths)

        if self.processes > 1:
            self.__scan_multiprocess(paths)
        elif self.threads > 1:
            self.__scan_multithread(paths)
        else:
            self.__scan_synchronous(paths)

    # Generate a list of files to scan
    def search_files(self, paths: Iterable[str]) -> Iterable[pathlib.Path]:
        for path in paths:
            # The pathlib glob seems not to be a true iterator
            # it appears to be taking up more memory than iglob
            # for f in glob.iglob(path + '/**', recursive=self.recursive):
            g = "**/*" if self.recursive else "*"
            for p in pathlib.Path(path).glob(g):
                if not p.is_file():
                    continue
                if self.skip_file(p):
                    continue
                else:
                    yield p

    # Filter files to scan
    def skip_file(self, p: pathlib.Path) -> bool:
        # Ignore by glob
        if self.ignore_glob:
            for g in self.ignore_glob:
                if p.match(g):
                    return True

        # Match by glob
        if self.match_glob:
            for g in self.match_glob:
                if p.match(g):
                    return False
            # If match globs are provided, and none was matched, then skip
            return True

        # If we got here, it's a keeper
        return False

    def __scan_synchronous(self, paths: Iterable[str]) -> None:
        for p in self.search_files(tuple(paths)):
            self.process_result(self.scan_file(p))

    def __scan_multiprocess(self, paths: Iterable[str]) -> None:
        # Ensure we clean up by using with statement
        # Use add_done_callback() to reap completed futures asynchronously
        with concurrent.futures.ProcessPoolExecutor(
            max_workers=self.processes
        ) as executor:
            for p in self.search_files(tuple(paths)):
                future = executor.submit(self.scan_file, p)
                future.add_done_callback(self.process_result)

    def __scan_multithread(self, paths: Iterable[str]) -> None:
        # Ensure we clean up by using with statement
        # Use add_done_callback() to reap completed futures asynchronously
        with concurrent.futures.ThreadPoolExecutor(
            max_workers=self.threads
        ) as executor:
            for p in self.search_files(tuple(paths)):
                future = executor.submit(self.scan_file, p)
                future.add_done_callback(self.process_result)

    def process_result(self, r: Union[str, concurrent.futures.Future]) -> None:
        if isinstance(r, concurrent.futures.Future):
            r = r.result()
        print(r)

    def scan_file(self, p: pathlib.Path) -> str:
        result = self.rules().match(str(p))
        if result:
            return f"MATCH: {p} => {result}"
        else:
            return f"SAFE: {p}"
