import yara # type: ignore
from pathlib import Path
from typing import Iterable
from yara_guard.YaraGuard import YaraGuardSources

class YaraRules:

    def __init__(self, s: Iterable[YaraGuardSources]):
        self.sources = s

    __glob: str = "**/*.yar*"

    @property
    def glob(self) -> str:
        return self.__glob

    @glob.setter
    def glob(self, s: str) -> str:
        self.__glob = s
        return self.__glob

    __sources: Iterable[YaraGuardSources] = ()
    @property
    def sources(self) -> Iterable[YaraGuardSources]:
        return self.__sources

    @sources.setter
    def sources(self, s: Iterable[YaraGuardSources]) -> Iterable[YaraGuardSources]:
        self.__sources = s
        return self.__sources


    def compile(self) -> yara.Rules:
        filepaths = {}
        for s in self.sources:
            for p in Path(s.directory).glob(self.glob):
                namespace = f"{s.name}_{p.with_suffix('').name}"
                filepaths[namespace] = str(p.absolute())

        return yara.compile(filepaths=filepaths)
