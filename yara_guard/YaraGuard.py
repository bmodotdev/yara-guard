import os
from dataclasses import dataclass
from typing import Dict, Iterable, List, Tuple, Union

import tomli


@dataclass
class YaraGuardSources:
    enabled: bool
    name: str
    directory: str


class YaraGuardException(Exception):
    """
    The parent class of all YaraGuard specific exceptions
    """

    prefix: str = "Error:"

    def __init__(self, msg: str = "Unknown", code: int = 127):
        self.msg = msg
        self.code = code

    def __str__(self) -> str:
        return "{prefix} {msg}".format(prefix=self.prefix, msg=self.msg)


# class YaraGuardConfigException(YaraGuardException):
#    prefix: str  = "Invalid Config:"
#    def __init__(self, *kwargs):
#        super().__init__(*kwargs)


class YaraGuardArgException(YaraGuardException):
    """
    An exception specifically concerning invalid arguments
    """

    prefix: str = "Invalid Argument:"

    def __init__(self, *kwargs):
        super().__init__(*kwargs)


class YaraGuard:
    """
    This parent class provides the base functionality for scanning with YaraGuard
    """

    def __init__(self, **kwargs):
        # Check for custom config file path
        if kwargs.get("config_file"):
            self.config_file = kwargs.pop("config_file")

        # Load from config file first
        self.load_config()

        # Apply args to object
        for key in kwargs:
            setattr(self, key, kwargs[key])

    # Config file
    __config_file: str = "/etc/yaraguard/config.toml"

    @property
    def config_file(self) -> str:
        """Returns the provided config file path"""
        return self.__config_file

    @config_file.setter
    def config_file(self, file: str) -> str:
        """
        Allows overriding the default config file path
        """

        if not os.path.isfile(file):
            raise YaraGuardArgException(f"Config file does not exist: {file}", 2)

        if not os.access(file, os.R_OK):
            raise YaraGuardArgException(f"Config file is not readable: {file}", 13)

        self.__config_file = file
        return self.__config_file

    # YARA sources
    __sources: Iterable[YaraGuardSources] = ()

    @property
    def sources(self) -> Iterable[YaraGuardSources]:
        """Returns the list of dictionaries containing YARA rule sources"""

        return self.__sources

    @sources.setter
    def sources(
        self, sources: Iterable[Dict]
    ) -> Iterable[YaraGuardSources]:
        """Allows setting the YARA rule sources"""

        self.__sources = (YaraGuardSources(**d) for d in sources)
        return self.__sources

    def load_config(self) -> None:
        """
        Loads the config file into this class
        """

        if not os.path.isfile(self.config_file):
            raise YaraGuardArgException(f"Config file does not exist: {self.config_file}", 2)

        if not os.access(self.config_file, os.R_OK):
            raise YaraGuardArgException(f"Config file is not readable: {self.config_file}", 13)

        with open(self.config_file, "rb") as fh:
            config = tomli.load(fh)

        # Apply our config
        for key in config:
            setattr(self, key, config[key])
