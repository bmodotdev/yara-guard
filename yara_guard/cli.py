import click

from yara_guard.commands import scan

help_text = "Watch and scan a filesystem with YARA rules"


@click.group(help=help_text)
@click.option(
    "--config-file",
    "-c",
    help="Path to config file",
    default="/etc/yaraguard/config.toml",
    show_default=True,
    required=False,
    type=click.Path(
        exists=False,
        file_okay=True,
        dir_okay=False,
        readable=True,
        resolve_path=True,
    ),
)
@click.option(
    "--verbose",
    "-v",
    help="Enable verbose output",
    required=False,
    default=False,
    show_default=False,
    is_flag=True,
)
@click.option(
    "--version",
    "-V",
    help="Print application version",
    required=False,
    default=False,
    show_default=False,
    is_flag=True,
)
@click.pass_context
def cli(ctx, config_file, verbose, version):
    ctx.obj = dict(
        config_file=config_file,
        verbose=verbose,
        version=version,
    )


cli.add_command(scan.scan)
