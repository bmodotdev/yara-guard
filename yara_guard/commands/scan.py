import os
import sys

import click

from yara_guard.YaraGuard import YaraGuardArgException
from yara_guard.YaraGuardScan import YaraGuardScan


@click.command()
@click.option(
    "--processes",
    "-p",
    help="Number of scanners to run in parallel",
    default=0,
    required=False,
    type=int,
    show_default=True,
)
@click.option(
    "--threads",
    "-t",
    help="Number of scanners to run in threads",
    default=os.cpu_count(),
    show_default=True,
    required=False,
    type=int,
)
@click.option(
    "--nice",
    "-n",
    help="Set scheduling priority, -20 to 19, highest to lowest priority",
    required=False,
    type=int,
)
@click.option(
    "--recursive",
    "-r",
    help="Scan directory recursively",
    default=False,
    required=False,
    is_flag=True,
    show_default=True,
)
@click.option(
    "--match-glob",
    "-m",
    help="Match files with glob(s), e.g. “-m '*.php' -m '*.py'",
    required=False,
    multiple=True,
)
@click.option(
    "--ignore-glob",
    "-i",
    help="Exclude files glob(s), e.g. “-i node_modules” ",
    required=False,
    multiple=True,
)
@click.option(
    "--quarantine-dir",
    "-D",
    help="Directory to move files matched by YARA rules",
    required=False,
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        readable=True,
        resolve_path=True,
    ),
)
@click.option(
    "--quarantine-chmod",
    "-P",
    help="Permissions to set on files matched by YARA rules",
    required=False,
    type=str,
)
@click.option(
    "--quarantine-chown",
    "-O",
    help="Owner and/or Group to set on files matched by YARA rules, i.e. “[user][:[group]]",
    required=False,
    type=str,
)
@click.argument(
    "paths",
    nargs=-1,
    required=True,
    type=click.Path(
        exists=True,
        file_okay=True,
        dir_okay=True,
        readable=True,
        resolve_path=True,
    ),
)
@click.pass_obj
def scan(ctx, **kwargs):
    # Remove args with None values
    for k in [k for k in kwargs.keys() if kwargs[k] is None]:
        kwargs.pop(k)

    # Pop our paths
    paths = kwargs.pop("paths")

    # Combine our global args with subcommand args
    kwargs.update(ctx)

    # Initialize our class
    try:
        ygs = YaraGuardScan(**kwargs)
    except YaraGuardArgException as e:
        # TODO: show help here
        print(e)
        sys.exit(e.code)
        return

    # Start the scan
    ygs.scan(paths)
